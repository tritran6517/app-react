import React, {Component} from 'react';
import './Form.scss';

class Form extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            email: "",
            tel: "",
            disabled: false
        }
        this.handleInput = this.handleInput.bind(this);
    }
    handleInput(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        return (
            <div className="c-form1">
                <h2><span>Đăng kí mở thẻ</span></h2>
                <form method="post">
                    <label className="c-form1__ttl">Tên đầy đủ</label>
                    <div className="c-form1__row">
                        <div className="c-form1__select">
                            <select>
                                <option>Cô/Chị</option>
                                <option>Bà</option>
                                <option>Ông</option>
                            </select>
                            <span></span>
                        </div>
                        <div className="c-form1__input">
                            <input
                                type="text"
                                placeholder="Tên đầy đủ"
                                name="name"
                                value={this.state.name}
                                onChange={this.handleInput}
                            />
                        </div>
                    </div>
                    <label className="c-form1__ttl">Email</label>
                    <div className="c-form1__row">
                        <div className="c-form1__input c-form1__input-email">
                            <input 
                                type="email"
                                placeholder="Điền vào đây"
                                name="email"
                                value={this.state.email}
                                onChange={this.handleInput}
                            />
                        </div>
                    </div>
                    <label className="c-form1__ttl">Điện thoại di động</label>
                    <div className="c-form1__row">
                        <div className="c-form1__select">
                            <select>
                                <option>+84</option>
                            </select>
                            <span></span>
                        </div>
                        <div className="c-form1__input">
                            <input
                                type="tel"
                                placeholder="Số điện thoại"
                                name="tel"
                                value={this.state.tel}
                                onChange={this.handleInput}
                            />
                        </div>
                    </div>
                    <div className="c-form1__btn">
                        <button 
                            disabled={(this.state.email && this.state.name && this.state.tel) ? this.state.disabled : !this.state.disabled}
                        ><span>tiếp tục</span></button>
                    </div>
                </form>
            </div>
        );
    }
}

export default Form;